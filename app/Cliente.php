<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use SoftDeletes;

    protected $table 	= 'clientes';
    protected $fillable = [ 'nombres' , 'apellido_paterno' , 'apellido_materno' , 'fecha_nacimiento' ];
    protected $dates 	= [ 'deleted_at' ];

    
}
