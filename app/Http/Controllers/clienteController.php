<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use App\Cliente;

class clienteController extends Controller
{
	/*public function __construct(){
		$this->middleware( 'cors' );
		$this->beforeFilter( '@find' , [ 'only'  => [ 'show' , 'update' , 'destroy' ] ] );
	}*/
	/**
	 * Display a listing of the resource.
	 * GET /cliente
	 *
	 * @return Response
	 */
	public function find( Route $route )
	{
		$this->cliente = Cliente::find( $route->getParameter('clientes ') );
	}
    /**
	 * Display a listing of the resource.
	 * GET /cliente
	 *
	 * @return Response
	 */
	public function index( $id = null )
	{
		/*5.2*/
		if ( $id == null ) {
			$clientes =  Cliente::orderBy( 'id' , 'asc' )->get();
			return  response()->json( $clientes->toArray() );
		} else {
			return $this->show( $id );
		}
		/*$clientes = Cliente::all();
		return  response()->json( $clientes->toArray() );*/
	}

	public function listar() {
		$clientes = Cliente::all();
		return  response()->json( $clientes->toArray() );
	}
	/**
	 * Store a newly created resource in storage.
	 * POST /cliente
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		/*5.2*/
		$cliente = new Cliente;

		$cliente->nombres 			= $request->input( 'nombres' );
		$cliente->apellido_materno 	= $request->input( 'apellido_materno' );
		$cliente->apellido_paterno 	= $request->input( 'apellido_paterno' );
		$cliente->fecha_nacimiento 	= $request->input( 'fecha_nacimiento' );
		$cliente->save();
		return response()->json(["mensaje"=>"Creado correctamente"]);
		/*Cliente::create( $request->all() );
		return response()->json(["mensaje"=>"creado correctamente"]);*/
	}

	/**
	 * Display the specified resource.
	 * GET /cliente/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		/*return response()->json($this->cliente);*/
		return Cliente::find( $id );
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /cliente/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$cliente = Cliente::find($id);
		$cliente->nombre 			= $request->input( 'nombres' );
		$cliente->apellido_materno 	= $request->input( 'apellido_materno' );
		$cliente->apellido_paterno 	= $request->input( 'apellido_paterno' );
		$cliente->fecha_nacimiento 	= $request->input( 'fecha_nacimiento' );
		$cliente->save();
		return response()->json(["mensaje"=>"Actualizado correctamente"]);
		/*$this->cliente->fill($request->all());
		$this->cliente->save();
		return response()->json( [ "mensaje"=>"Cliente Actualizado" ] );*/
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /cliente/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$cliente = Cliente::find($id);
		$cliente->delete();
		return response()->json(["mensaje"=>"Eliminado correctamente"]);
	}
}
