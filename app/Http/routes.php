<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource( 'clientes' , 'clienteController' );
//Route::resource('/hola/{id}', 'clienteController@index');
Route::get( 	'clientes/{id?}' 	, 'clienteController@index');
Route::resource( 'clientes' 		, 'clienteController@listar' );
Route::post( 	'clientes' 			, 'clienteController@store');
Route::put( 	'clientes/{id}' 	, 'clienteController@update');
Route::delete( 	'clientes/{id}' 	, 'clienteController@destroy');