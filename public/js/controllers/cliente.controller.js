/**
    *   This controller function is create a new customer
    *   @author     Cesar Herrera <kyele936@gmail.com>
    *   @since      30/08/2016
    *   @version    1
    *   @access     public
    *   @param      Service [$resource]
    *   @param      Service [$location]
    *   @param      Service [routeServices]
    *   @return     
    *   @example    cliente.listar( .... )
*/
angular.module( 'appBioxor' )
    .controller( 'clienteCreateCtrl' , clienteCreateCtrl );

clienteCreateCtrl.$inject = [ '$state' , '$scope' , 'clienteServices' ];

function clienteCreateCtrl( $state , $scope , clienteServices ) {

    var cliente             = this;
    cliente.datos           = {};
    cliente.saveCustomer    = function() {
        //clienteServices.save(cliente.datos);
        //swal("Guardado!", "Se ha guardado correctamente la informacion!", "success");
        //$state.go('cliente');
        //console.log(cliente.datos);
        clienteServices.agregar( cliente.datos ,
            function( data ) {
                console.log( data.mensaje );
                swal("Guardado!", "Se ha guardado correctamente la informacion!", "success");
                $state.go("cliente");
            }, function( data ) {
                console.log( data.message );
            }
        );
    }
};
/**
    *   This controller function is show total customers
    *   @author     Cesar Herrera <kyele936@gmail.com>
    *   @since      31/08/2016
    *   @version    1
    *   @access     public
    *   @param      Service [$resource]
    *   @param      Service [$location]
    *   @param      Service [routeServices]
    *   @return     data
    *   @example    clienteListCtrl.query( .... )
*/
angular.module( 'appBioxor' )
    .controller( 'clienteListCtrl' , clienteListCtrl );

clienteListCtrl.$inject = [ '$state' , '$scope' , 'clienteServices' ];

function clienteListCtrl( $state , $scope , clienteServices ) {

    var cliente             = this;
    /*cliente.datos           = clienteServices.query();*/
    cliente.datos           = [];
    cliente.datos           = clienteServices.listar()
    console.log(cliente.datos);
    cliente.editar          = function( id ) {
        console.log(id);
        $state.go( 'cliente_edit' , {id : id} );
    };
};
/**
    *   This controller function is edit a customer information
    *   @author     Cesar Herrera <kyele936@gmail.com>
    *   @since      31/08/2016
    *   @version    1
    *   @access     public
    *   @param      Service [$resource]
    *   @param      Service [$location]
    *   @param      Service [routeServices]
    *   @return     data
    *   @example    cliente.editar( .... )
*/
angular.module( 'appBioxor' )
    .controller( 'clienteEditCtrl' , clienteEditCtrl );

clienteEditCtrl.$inject = [ '$state' , '$scope' , 'clienteServices' , '$location' , '$routeParams' ];

function clienteEditCtrl( $state , $scope , clienteServices , $location , $routeParams ) {

    var cliente             = this;
    clienteServices.mostrar( $stateParams.id , 
        function( data ) {
            console.log(data);
        }
    );
    console.log(cliente.datos);
};