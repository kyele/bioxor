angular.module('appBioxor', [
    'ngResource' , 'ui.router'
])
.config(configuration)
.run(run);

function configuration( $stateProvider , $urlRouterProvider ) {
    'use strict';

    $urlRouterProvider
        .when('/', '/inicio')
        .otherwise('/inicio');

    $stateProvider
        .state('pagina', {
            url:            '/',
            templateUrl:    'views/pagina.html',
            /*controller: 'dashboardController',
            controllerAs: 'vm'*/
        })
            .state('inicio', {
                parent:         'pagina',
                url:            '^/inicio',
                templateUrl:    'views/inicio.html'
            })
            .state('cliente_create', {
                parent:         'pagina',
                url:            '^/nuevo_cliente',
                templateUrl:    'views/cliente_alta.html',
                controller:     'clienteCreateCtrl',
                controllerAs:   'cliente'
            })
            .state('cliente_list', {
                parent:         'pagina',
                url:            '^/clientes',
                templateUrl:    'views/cliente_list.html',
                controller:     'clienteListCtrl',
                controllerAs:   'cliente'
            })
            .state('cliente_edit', {
                parent:         'pagina',
                url:            '^/clientes/editar/:id',
                templateUrl:    'views/cliente_form.html',
                controller:     'clienteEditCtrl',
                controllerAs:   'cliente'
            })
    };

function run($state) {
    'use strict';
    $state.go('pagina');
}
