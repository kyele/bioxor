<!DOCTYPE html>
<html ng-app="appBioxor">
    <head>
        <meta charset="UTF-8">
        <title>Bioxor</title>
        <meta name="description" content="CRUD prueba Bioxor" />
        <meta name="keywords" content="CRUD , Bioxor" />

        <meta name="author" content="Cesar Herrera" />
        <meta name="copyright" content="CUCEI" />

        <link rel="stylesheet" type="text/css" href="dependencies/bootstrap/dist/css/bootstrap-theme.css" />
        <link rel="stylesheet" type="text/css" href="dependencies/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="dependencies/angular-bootstrap/ui-bootstrap-csp.css" />
        <link rel="stylesheet" type="text/css" href="dependencies/font-awesome/css/font-awesome.css" />
        <link rel="stylesheet" type="text/css" href="dependencies/bootstrap-sweetalert/lib/sweet-alert.css">

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div ui-view>
        </div>
        <!-- Latest compiled and minified JavaScript -->
        <script type="text/javascript" src="dependencies/jquery/dist/jquery.js"></script>
        <script type="text/javascript" src="dependencies/bootstrap/dist/js/bootstrap.js"></script>
        <script type="text/javascript" src="dependencies/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script type="text/javascript" src="dependencies/angular/angular.js"></script>
        <script type="text/javascript" src="dependencies/angular-bootstrap/ui-bootstrap.js"></script>
        <script type="text/javascript" src="dependencies/angular-bootstrap/ui-bootstrap-tpls.js"></script>
        <script type="text/javascript" src="dependencies/angular-ui-router/release/angular-ui-router.js"></script>
        <script type="text/javascript" src="dependencies/angular-resource/angular-resource.min.js"></script>

        <!-- Import  app js -->
        <script type="text/javascript" src="js/main.js"></script>
        <!-- Import  controllers -->
        <script type="text/javascript" src="js/controllers/cliente.controller.js"></script>
        <!-- Import  services -->
        <script type="text/javascript" src="js/services/cliente.service.js"></script>
    </body>
</html>